﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unit2
{
    class Program
    {
        static void Main(string[] args)
        {
            //ThreeNumbers();
            //Seasons();
            //Foreach();
            //Condition();
            Sum();
            Console.ReadKey();
        }

        static void Sum()
        {
            Console.WriteLine("Введите число:");
            string s = Console.ReadLine();
            if (!int.TryParse(s, out int n) || n < 0)
            {
                Console.WriteLine("Это не целое число или отрицательное число");
            }
            int sum = 0;
            for (int i = 1; i <= n; i++)
            {
                sum += i;
            }
            Console.WriteLine($"Сумма равна: {sum}");
        }

        private static void Seasons()
        {
            string season = Console.ReadLine();
            switch (season)
            {
                case "Spring": Console.WriteLine("Весна"); break;
                case "Summer": Console.WriteLine("Лето"); break;
                case "Autumn": Console.WriteLine("Осень"); break;
                default: Console.WriteLine("Зима"); break;
            }

        }

        private static void ThreeNumbers()
        {
            string s = Console.ReadLine();
            char begin = s[0];
            char middle = s[1];
            char end = s[2];
            string a = end.ToString() + middle.ToString() + begin.ToString();
            bool b = int.TryParse(a, out int i);
            Console.WriteLine(b ? a : "Это не число!");
            //if (b)
            //{
            //    Console.WriteLine(a);
            //}
            //else
            //{
            //    Console.WriteLine("Это не число!");
            //}
        } 

        static void Foreach()
        {
            int[] arr = new int[5] { 0, 1, 2, 3, 4 };
            foreach (int i in arr)
            {
                if (i == 2) continue;
                Console.WriteLine("\"\n");
            }
        }

        static void Condition()
        {
            Console.WriteLine("Введите строку:");
            string s = Console.ReadLine();
            string sUpper = s.ToUpper();
            bool b = false;
            if (sUpper.StartsWith("A"))
            {
                b = true;
            }
            Console.WriteLine(b); //? "True" : "False");
        }
    }
}
